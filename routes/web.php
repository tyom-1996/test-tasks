<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware ( 'manager' )->group ( function () {
    Route::get ( '/manager' , '\App\Http\Controllers\manager\ManagerController@index' )->name ( 'manager' );
    Route::get ( '/manager/create/user' , '\App\Http\Controllers\manager\ManagerController@createUser' )->name ( 'create.user' );
    Route::post ( '/manager/store/user' , '\App\Http\Controllers\manager\ManagerController@storeUser' )->name ( 'store.user' );
    Route::get ( '/manager/category/{id}' , '\App\Http\Controllers\manager\ManagerController@category_view' )->name ( 'catgory.manager' );
    Route::post ( '/manager/delete/post/{id}' , '\App\Http\Controllers\manager\ManagerController@delete' )->name ( 'delete.post.manager' );
});

Route::middleware ( 'employee' )->group ( function () {
    Route::get ( '/home' , '\App\Http\Controllers\employee\employeeController@index' )->name ( 'home_user' );
    Route::get ( '/create/post' , '\App\Http\Controllers\employee\employeeController@create' )->name ( 'create.post' );
    Route::post ( '/store/post' , '\App\Http\Controllers\employee\employeeController@store' )->name ( 'store.post' );
    Route::get ( '/create/post/{id}/edit' , '\App\Http\Controllers\employee\employeeController@edit' )->name ( 'edit.post' );
    Route::post ( '/update/post/{id}' , '\App\Http\Controllers\employee\employeeController@update' )->name ( 'update.post' );
    Route::post ( '/delete/post/{id}' , '\App\Http\Controllers\employee\employeeController@delete' )->name ( 'delete.post' );

    Route::get ( '/category/{id}' , '\App\Http\Controllers\employee\employeeController@category_view' )->name ( 'catgory' );
});

Route::redirect ( '/' , 'login' );
