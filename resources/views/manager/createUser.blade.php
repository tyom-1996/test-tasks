@extends ( 'layouts.app' )

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <div class="card_title">
                        {{ __('Dashboard') }}
                    </div>
                    <div class="card_button">
                        <a class="btn btn-primary" href="{{ route ( 'create.user') }}" role="button">Create employee</a>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{ route ( 'store.user') }}" method="POST">
                        @csrf
                        <div class="mb-3 row">
                            <label for="static" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="static" name="name">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                            <input type="email" class="form-control" id="staticEmail" name="email">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                            <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputPassword" name="password">
                            </div>
                        </div>
                        <input type="hidden" name="parent" value="{{ auth()->user()->id }}">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <button type="submit" class="btn btn-primary">Primary</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection