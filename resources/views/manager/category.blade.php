@extends ( 'layouts.app' )

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <div class="card_title">
                        {{ __('Dashboard') }}
                    </div>
                </div>

                <div class="card-body d-flex flex-wrap">
                    @foreach ( $posts as $item )
                        <div class="card m-1" style="width: 18rem;">
                            <img src="{{ asset ( $item->image ) }}" class="card-img-top" alt="{{ $item->title }}">
                            <div class="card-body">
                                <h5 class="card-title">{{ $item->title }}</h5>
                            </div>
                            <div class="card-body">
                                <div class="card-title">
                                    Author : {{ App\Http\Controllers\manager\ManagerController::user_name ( $item->author )['name'] }}
                                </div>
                            </div>
                            <ul class="list-group list-group-flush d-flex justify-content-between align-items-center p-4">
                                Category: <a href="{{ route ( 'catgory.manager' , $item->catgeroies ) }}">{{ App\Http\Controllers\employee\employeeController::catgeroy ( $item->catgeroies )['title'] }}</a>
                            </ul>
                            <div class="card-body d-flex align-items-center">
                                <form action="{{ route ( 'delete.post.manager' , $item->id ) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-link">
                                    Delete post
                                    </button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
                {{ $posts->links() }}
            </div>
        </div>
    </div>
</div>
@endsection