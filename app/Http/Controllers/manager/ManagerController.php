<?php

namespace App\Http\Controllers\manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\User;
use App\User as Users;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Posts;
use File;

class ManagerController extends Controller
{
    public function index () {
        $user = Auth::user()->id;
        $posts = Posts::join ( 'users' , 'posts.author' , 'users.id' )->where ( 'users.parent' , $user )
        ->select ( 'posts.id' , 'title' , 'image' , 'author' , 'name' , 'catgeroies' )->paginate ( 10 );
        $users = Users::where ( 'parent' , $user )->get ();

        return view ( 'manager.index' , compact ( 'users' , 'posts' ) );
    }

    public function createUser () {
        return view ( 'manager.createUser' );
    }

    public function storeUser ( User $request ) {
        $user = new Users;
        $user->password = Hash::make ( $request->password );
        $user->email = $request->email;
        $user->name = $request->name;
        $user->role = 1;
        $user->parent = $request->parent;
        $user->save();
        
        return redirect ()->route ( 'manager' )->with('message', 'Հաջողությամբ կատարված է!');
    }

    public function category_view ( $id ) {

        $user = Auth::user()->id;

        $posts = Posts::join ( 'users' , 'posts.author' , 'users.id' )->where ( 'users.parent' , $user )->where ( 'posts.catgeroies' , $id )->paginate ( 10 );

        return view ( 'manager.category' , compact ( 'posts' ) );
    }

    public static function user_name ( $id ) {
        return Users::find ( $id );
    }

    public function delete ( $id ) {

        $resurs = Posts::find ( $id );
        
        File::delete ( $resurs->image );

        $resurs->delete ();

        return redirect ()->route ( 'manager' )->with('message', 'Post deleted');
        
    }
}
