<?php

namespace App\Http\Controllers\employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Posts;
use App\Category;
use App\Http\Requests\Posts as PostsRequest;
use Auth;
use App\Http\Requests\PostEdit;
use File;

class employeeController extends Controller
{
    public function index () {
        $posts = Posts::where ( 'author' , '=' , Auth::user()->id )->paginate ( 10 );
        return view ( 'employee.index' , compact ( 'posts' ) );
    }

    public function create () {
        $category = Category::all ();
        return view ( 'employee.create' , compact ( 'category' ) );
    }

    public function store ( PostsRequest $request ) {
        $data = $request->validated ();
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        $data['image'] = request()->image->move ( 'images' , $imageName);
        $data['image'] = $data['image']->getPath().'/'.$data['image']->getFileName();
        $data['author'] = Auth::user()->id;
        Posts::firstOrCreate ( $data );

        return redirect ()->route ( 'home_user' )->with('message', 'Post created');
    }

    public function edit ( $id ) {

        $resurs = Posts::find ( $id );

        $category = Category::all ();

        return view ( 'employee.edit' , compact ( 'resurs' , 'category' ) );
        
    }

    public function update ( PostEdit $request , $id ) {
        $data = $request->validated ();

        $resurs = Posts::find ( $id );

        if ( isset ( $data['image'] ) ) {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            $data['image'] = request()->image->move ( 'images' , $imageName);
            $data['image'] = $data['image']->getPath().'/'.$data['image']->getFileName();
        }

        $data['author'] = Auth::user()->id;

        $resurs->update ( $data );

        return redirect ()->route ( 'home_user' )->with('message', 'Post created');
    }

    public function delete ( $id ) {

        $resurs = Posts::find ( $id );

        
        File::delete ( $resurs->image );

        $resurs->delete ();

        return redirect ()->route ( 'home_user' )->with('message', 'Post deleted');
        
    }

    public function category_view ( $id ) {

        $user = Auth::user()->id;

        $posts = Posts::where ( 'catgeroies' , $id )->where ( 'author' , $user )->paginate ( 10 );

        return view ( 'employee.category' , compact ( 'posts' ) );
    }

    public static function catgeroy ( $id ) {
        return Category::find ( $id );
    }
}
