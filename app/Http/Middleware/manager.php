<?php

namespace App\Http\Middleware;

use Closure;

class manager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( (int) auth ()->user ()->role !== 0 ) {
            abort ( 404 );
        }
        return $next($request);
    }
}
