<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(array (
            [
                'title' => Str::random ( 10 ),
            ],
            [
                'title' => Str::random ( 10 ),
            ],
            [
                'title' => Str::random ( 10 ),
            ],
            [
                'title' => Str::random ( 10 ),
            ]
        ));
    }
}
